from pydantic import BaseModel
from typing import List


class ContactInfo(BaseModel):
    phone: str
    email: str


class ContactInfoResponse(BaseModel):
    phone: str
    email: str


class RestaurantData(BaseModel):
    name: str


class RestaurantCreate(BaseModel):
    name: str
    location: str
    cuisine: str
    rating: float
    phone: str
    email: str


class RestaurantResponse(BaseModel):
    id: int
    name: str
    location: str
    cuisine: str
    rating: float
    contact: ContactInfoResponse
