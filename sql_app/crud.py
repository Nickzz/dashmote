from sqlalchemy.orm import Session
from typing import Optional
from . import models, schemas


def view_restaurants(db: Session):
    return db.query(models.Restaurant).all()


def get_restaurant_by_id(db: Session, restaurant_id: int):
    return (
        db.query(models.Restaurant)
        .filter(models.Restaurant.id == restaurant_id)
        .first()
    )


def add_restaurants(db: Session, restaurant_data: schemas.RestaurantCreate):
    new_contact = models.ContactInfo(
        phone=restaurant_data.phone, email=restaurant_data.email
    )
    new_restaurant = models.Restaurant(
        name=restaurant_data.name,
        location=restaurant_data.location,
        cuisine=restaurant_data.cuisine,
        rating=restaurant_data.rating,
        contact=new_contact,  # Establish the relationship
    )
    # Add the new restaurant to the session
    db.add(new_restaurant)
    # Commit the transaction
    db.commit()
    # Refresh to fetch the latest data including any defaults or IDs set by the database
    db.refresh(new_restaurant)

    return {
        "id": new_restaurant.id,
        "name": new_restaurant.name,
        "location": new_restaurant.location,
        "cuisine": new_restaurant.cuisine,
        "rating": new_restaurant.rating,
        "contact": {
            "phone": new_restaurant.contact.phone,
            "email": new_restaurant.contact.email,
        },
    }


def update_restaurant(
    db: Session, restaurant_id: int, update_data: schemas.RestaurantCreate
):
    restaurant = (
        db.query(models.Restaurant)
        .filter(models.Restaurant.id == restaurant_id)
        .first()
    )
    if restaurant:
        if update_data.name is not None:
            restaurant.name = update_data.name
        if update_data.location is not None:
            restaurant.location = update_data.location
        if update_data.cuisine is not None:
            restaurant.cuisine = update_data.cuisine
        if update_data.rating is not None:
            restaurant.rating = update_data.rating

        # Update contact info attributes
        if update_data.phone is not None:
            restaurant.contact.phone = update_data.phone
        if update_data.email is not None:
            restaurant.contact.email = update_data.email

        db.commit()
        return {
            "id": restaurant.id,
            "name": restaurant.name,
            "location": restaurant.location,
            "cuisine": restaurant.cuisine,
            "rating": restaurant.rating,
            "contact": {
                "phone": restaurant.contact.phone,
                "email": restaurant.contact.email,
            },
        }
    return None


def delete_restaurant(db: Session, restaurant_id: int) -> bool:
    restaurant = (
        db.query(models.Restaurant)
        .filter(models.Restaurant.id == restaurant_id)
        .first()
    )
    if restaurant:
        db.delete(restaurant)
        db.commit()
        return True
    return False


def get_restaurants_by_filters(
    db: Session, location: Optional[str], cuisine: Optional[str]
):
    query = db.query(models.Restaurant)
    if location:
        query = query.filter(models.Restaurant.location == location)
    if cuisine:
        query = query.filter(models.Restaurant.cuisine == cuisine)
    print("query")
    return query.all()
