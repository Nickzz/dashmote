import logging
from fastapi import Depends, FastAPI, HTTPException, status, Query
from sqlalchemy.orm import Session
from typing import Optional, List

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/restaurants/", response_model=List[schemas.RestaurantData])
def read_restaurants(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    """
    Retrieves a list of all restaurants from the database.
    Returns a list of restaurant data including names and associated details.
    """
    restaurants = db.query(models.Restaurant).offset(skip).limit(limit).all()
    response = [{"name": restaurant.name} for restaurant in restaurants]
    return response


@app.get("/restaurants/{restaurant_id}", response_model=schemas.RestaurantResponse)
def get_restaurant(restaurant_id: int, db: Session = Depends(get_db)):
    """
    Retrieves a single restaurant by its ID.

    - **restaurant_id**: int - Unique identifier of the restaurant
    Returns detailed information about a specific restaurant, including its
                                    location, cuisine, and contact information.
    """
    restaurant = crud.get_restaurant_by_id(db, restaurant_id)
    if not restaurant:
        raise HTTPException(status_code=404, detail="Restaurant not found")
    return {
        "id": restaurant.id,
        "name": restaurant.name,
        "location": restaurant.location,
        "cuisine": restaurant.cuisine,
        "rating": restaurant.rating,
        "contact": {
            "phone": restaurant.contact.phone,
            "email": restaurant.contact.email,
        },
    }


@app.post(
    "/restaurants/",
    response_model=schemas.RestaurantResponse,
    status_code=status.HTTP_201_CREATED,
)
def create_restaurant(
    restaurant: schemas.RestaurantCreate, db: Session = Depends(get_db)
):
    """
    Creates a new restaurant in the database.

    - **restaurant**: RestaurantCreate -
                        A model representing the restaurant to be added

    Adds a restaurant to the database
                        and returns the newly created restaurant data.
    """
    # add a new restaurant
    try:
        db_restaurant = crud.add_restaurants(db, restaurant)
        return db_restaurant
    except Exception as e:
        logging.error(f"Error occurred: {e}")
        raise HTTPException(status_code=400, detail=str(e))


@app.put("/restaurants/{restaurant_id}", response_model=schemas.RestaurantResponse)
def update_restaurant_details(
    restaurant_id: int,
    update_data: schemas.RestaurantCreate,
    db: Session = Depends(get_db),
):
    """
    Updates the details of an existing restaurant.

    - **restaurant_id**: int - The ID of the restaurant to update
    - **update_data**: RestaurantCreate -
                        The data to update the restaurant with
    Updates specified fields of a restaurant and
                        returns the updated restaurant details.
    """
    updated_restaurant = crud.update_restaurant(db, restaurant_id, update_data)
    if not updated_restaurant:
        raise HTTPException(status_code=404, detail="Restaurant not found")
    return updated_restaurant


@app.delete("/restaurants/{restaurant_id}")
def delete_restaurant(restaurant_id: int, db: Session = Depends(get_db)):
    """
    Deletes a restaurant from the database.

    - **restaurant_id**: int - The ID of the restaurant to delete
    Successfully deletes a restaurant and returns a confirmation message.
    """
    success = crud.delete_restaurant(db, restaurant_id)
    if success:
        return {"message": "deleted successfully"}
    else:
        raise HTTPException(status_code=404, detail="Restaurant not found")


@app.get("/filter/restaurants", response_model=List[schemas.RestaurantResponse])
def get_restaurants(
    location: Optional[str] = Query(None),
    cuisine: Optional[str] = Query(None),
    db: Session = Depends(get_db),
):
    """
    Filters restaurants based on location and/or cuisine.

    - **location**: Optional[str] -
                    Filter by the geographical location of the restaurant
    - **cuisine**: Optional[str] -
                    Filter by the type of cuisine offered by the restaurant
    Returns a list of restaurants that match the filter criteria.
    """
    restaurants = crud.get_restaurants_by_filters(
        db, location=location, cuisine=cuisine
    )
    response = [
        {
            "id": restaurant.id,
            "name": restaurant.name,
            "location": restaurant.location,
            "cuisine": restaurant.cuisine,
            "rating": restaurant.rating,
            "contact": {
                "phone": restaurant.contact.phone,
                "email": restaurant.contact.email,
            },
        }
        for restaurant in restaurants
    ]
    return response
