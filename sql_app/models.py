from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship

from .database import Base


class Restaurant(Base):
    __tablename__ = "restaurants"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    location = Column(String)
    cuisine = Column(String)
    rating = Column(Float)
    # Explicitly specify foreign_keys in the relationship
    contact = relationship(
        "ContactInfo",
        back_populates="restaurant",
        uselist=False,
        cascade="all, delete, delete-orphan",
    )


class ContactInfo(Base):
    __tablename__ = "contact_info"
    id = Column(Integer, primary_key=True)
    phone = Column(String)
    email = Column(String)
    restaurant_id = Column(Integer, ForeignKey("restaurants.id", ondelete="CASCADE"))
    restaurant = relationship(
        "Restaurant", back_populates="contact", passive_deletes=True
    )
