"""a

Revision ID: 6949367ab9bc
Revises: 4716162e3f98
Create Date: 2024-06-16 01:42:51.123380

"""

from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = "6949367ab9bc"
down_revision: Union[str, None] = "4716162e3f98"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        "contact_info",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("phone", sa.String, nullable=False),
        sa.Column("email", sa.String),
        sa.Column("restaurant_id", sa.String),
    )


def downgrade() -> None:
    op.drop_table("contact_info")
