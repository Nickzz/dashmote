"""drop contact_id column in restaurants

Revision ID: 6907b939847c
Revises:
Create Date: 2024-06-16 01:38:33.397216

"""

from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = "4716162e3f98"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        "restaurants",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String, nullable=False),
        sa.Column("location", sa.String),
        sa.Column("cuisine", sa.String),
        sa.Column("rating", sa.Float),
    )


def downgrade() -> None:
    op.drop_table("restaurants")
